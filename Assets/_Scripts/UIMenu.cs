﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using UnityEngine;
using UnityEngine.Networking.Match;
using UnityEngine.UI;

public class UIMenu : MonoBehaviour {

	private NetworkMaganer networkMaganer;


	public GameObject pnNamePlayer;
	public GameObject pnMenu,pnListRoom;
	public GameObject pnCreate;
	public InputField ipNamePlayer;
	public InputField ipNameRoom;
	public InputField ipNumsPlayer;

	public static string namePlayer;
	
	void Start () {
		networkMaganer = NetworkMaganer.Ins();
	}
	private void OnEnable()
	{
		pnMenu.SetActive(false);
		pnCreate.SetActive(false);
		pnListRoom.SetActive(false);
		pnNamePlayer.SetActive(true);
	}
	// Update is called once per frame
	void Update () {
	}
	
	public void OnJoinGame()
	{
		networkMaganer.StartMatchMaker();
		networkMaganer.ShowUI(pnListRoom);
	}
	public void OnCreateGame()
	{
		networkMaganer.StartMatchMaker();
		networkMaganer.matchMaker.CreateMatch(
			ipNameRoom.text,
			 (uint)4,
			true,
			"", "", "", 0, 0,
			networkMaganer.OnMatchCreate
		);
	}
	public void OnOpenMenu()
	{
		namePlayer = ipNamePlayer.text;
		pnNamePlayer.SetActive(false);
		pnMenu.SetActive(true);
	}
	public void OnOpenCreate()
	{
		pnMenu.SetActive(false);
		pnCreate.SetActive(true);
	}
	public void SetDefaults()
	{
		gameObject.SetActive(false);
		pnMenu.SetActive(false);
		pnCreate.SetActive(false);
		pnNamePlayer.SetActive(true);
	}
	public static string GetLocalIPAddress()
	{
		var host = Dns.GetHostEntry(Dns.GetHostName());
		foreach (var ip in host.AddressList)
		{
			if (ip.AddressFamily == AddressFamily.InterNetwork)
			{
				return ip.ToString();
			}
		}
		return "";
	}
}
