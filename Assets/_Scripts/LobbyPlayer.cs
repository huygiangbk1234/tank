﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class LobbyPlayer :  NetworkLobbyPlayer{

	[SyncVar(hook = "OnChangeReady")]
	public bool isReady;
	public Text txName;
	public Text txReady;
	[SyncVar(hook = "OnJoinTeam")]
	public int team=-1;
	public Color colorTeam;
	[SyncVar(hook = "OnChangeName")]
	public string nameP="";
	//gọi mỗi khi có 1 máy vào phòng
	public override void OnClientEnterLobby()
	{
		base.OnClientEnterLobby();
		if (isLocalPlayer) return;
		OnChangeName(nameP);
		OnJoinTeam(team);
		OnChangeReady(isReady);
	}
	public override void OnStartAuthority()
	{
		base.OnStartAuthority();
		CmdChangeName(UIMenu.namePlayer);
		UIRoom.ins.player = this;
	}
	[Command]
	public void CmdChangeName(string name)
	{
		nameP = name;
	}
	public void OnChangeName(string n)
	{
		txName.text = n;
	}
	[Command]
	public void CmdChangeTeam()
	{
		if (team == 1)
		{
			team = 0;
			NetworkMaganer.Ins().numsPlayerRed--;
			NetworkMaganer.Ins().numsPlayerBlue++;
		}
		else
		{
			team = 1;
			NetworkMaganer.Ins().numsPlayerRed++;
			NetworkMaganer.Ins().numsPlayerBlue--;
		}
	}
	public void OnJoinTeam(int n)
	{
		UIRoom.ins.AddTeam(n, gameObject);
	}
	public void OnChangeTeam()
	{
		CmdChangeTeam();
	}
	[Command]
	public void CmdChangeReady(bool b)
	{
		isReady = b;
	}
	public void OnToggleReady()
	{
		if (!isReady) SendReadyToBeginMessage();
		else SendNotReadyToBeginMessage();
		CmdChangeReady(!isReady);
	}
	public void OnChangeReady(bool b)
	{
		if (b) txReady.text = "Ready";
		else txReady.text = "Not Ready";
	}
}
