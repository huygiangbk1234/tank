﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;

public class NetworkMaganer : NetworkLobbyManager
{

	private static NetworkMaganer ins;
	public GameObject pnMenu, pnRoom;
	private GameObject currentPn;

	public int numsPlayerBlue = 0;
	public int numsPlayerRed = 0;
	private ulong currentMatchID;
	private int n;
	private void Awake()
	{
		if (ins == null)
		{
			DontDestroyOnLoad(gameObject);
			ins = this;
		}
		else
		{
			Destroy(this);
		}
	}
	public static NetworkMaganer Ins() { return ins; }
	void Start () {
		ShowUI(pnMenu);
	}
	public void JoinMatch(UnityEngine.Networking.Types.NetworkID networkID)
	{
		matchMaker.JoinMatch(networkID, "", "", "", 0, 0, OnMatchJoined);
	}
	public void ShowUI(GameObject ob)
	{
		if (currentPn != null)
		{
			currentPn.SetActive(false);
		}
		if (ob != null) ob.SetActive(true);
		currentPn = ob;
	}
	public override void OnLobbyStartServer()
	{
		print("server ready");
		base.OnLobbyStartServer();
	}
	public override void OnMatchCreate(bool success, string extendedInfo, UnityEngine.Networking.Match.MatchInfo matchInfo)
	{
		base.OnMatchCreate(success, extendedInfo, matchInfo);
		currentMatchID = (System.UInt64)matchInfo.networkId;
	}
	public override void OnLobbyServerConnect(NetworkConnection conn)
	{
		base.OnLobbyServerConnect(conn);
	}
	public override void OnLobbyClientConnect(NetworkConnection conn)
	{
		print("client connected");
		base.OnLobbyClientConnect(conn);
		ShowUI(pnRoom);
	}
	public override void OnLobbyServerPlayerRemoved(NetworkConnection conn, short playerControllerId)
	{
		base.OnLobbyServerPlayerRemoved(conn, playerControllerId);
		n--;
		if (n == 0)
		{
			matchMaker.DestroyMatch((UnityEngine.Networking.Types.NetworkID)currentMatchID, 0, OnDestroyMatch);
		}
	}
	public override void OnStartHost()
	{
		base.OnStartHost();
		print("create host");
	}
	public override GameObject OnLobbyServerCreateLobbyPlayer(NetworkConnection conn, short playerControllerId)
	{
		n++;
		GameObject obj = Instantiate(lobbyPlayerPrefab.gameObject) as GameObject;
		LobbyPlayer newPlayer = obj.GetComponent<LobbyPlayer>();
		if (numsPlayerBlue > numsPlayerRed)
		{
			newPlayer.team=1;
			numsPlayerRed++;
		}
		else
		{
			newPlayer.team = 0;
			numsPlayerBlue++;
		}
		return obj;
	}
	public override void OnServerSceneChanged(string sceneName)
	{
		print("OnServerSceneChanged");
		base.OnServerSceneChanged(sceneName);
	}
	public override void OnLobbyClientSceneChanged(NetworkConnection conn)
	{
		ShowUI(null);
		print("OnLobbyClientSceneChanged");
		base.OnLobbyClientSceneChanged(conn);
	}
	public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId)
	{
		print("OnServerAddPlayer");
		base.OnServerAddPlayer(conn, playerControllerId);
	}
	public override bool OnLobbyServerSceneLoadedForPlayer(GameObject lobbyPlayer, GameObject gamePlayer)
	{
		print("OnLobbyServerSceneLoadedForPlayer");
		GameMaganer.AddTank(gamePlayer);
		return true;
	}
	public override GameObject OnLobbyServerCreateGamePlayer(NetworkConnection conn, short playerControllerId)
	{
		print("server:create player");
		GameObject obj = Instantiate(gamePlayerPrefab.gameObject) as GameObject;
		return obj;
	}
	public override void OnClientDisconnect(NetworkConnection conn)
	{
		ShowUI(pnMenu);
		base.OnClientDisconnect(conn);
	}
}
