﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class UIRoom : MonoBehaviour {

	public static UIRoom ins;
	public Transform blueTeam, redTeam;
	public LobbyPlayer player;
	public Button btReady;
	public GameObject pnListRoom;
	private void OnEnable()
	{
		ins = this;
	}
	public void AddTeam(int i,GameObject ob)
	{
		if(i==1) ob.transform.SetParent(redTeam);
		else if (i == 0) ob.transform.SetParent(blueTeam);
	}
	public void OnChangeTeam()
	{
		if (player != null)
			player.OnChangeTeam();
	}
	public void OnToogleReady()
	{
		if (player == null) return;
		player.OnToggleReady();
		if (player.isReady) btReady.GetComponentInChildren<Text>().text = "Cancel Ready";
		else btReady.GetComponentInChildren<Text>().text = "Ready";
	}
	public void OnExitRoom()
	{
		player.RemovePlayer();
		NetworkMaganer.Ins().ShowUI(pnListRoom);
	}
}
