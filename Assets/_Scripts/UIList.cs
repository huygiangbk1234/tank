﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking.Match;
using UnityEngine.UI;

public class UIList : MonoBehaviour {

	public GameObject match;
	public Transform content;
	private MatchInfoSnapshot matchSelect;
	private void OnEnable()
	{
		NetworkMaganer.Ins().matchMaker.ListMatches(0, 5, "", true, 0, 0, OnShowList);
	}

	public void OnShowList(bool success, string extendedInfo, List<MatchInfoSnapshot> matches)
	{
		foreach (Transform t in content) Destroy(t.gameObject);
		for (int i=0;i<matches.Count;i++)
		{
			GameObject ob = Instantiate(match);
			MatchInfoSnapshot m = matches[i];
			int k = i;
			ob.GetComponentsInChildren<Text>()[0].text = m.name;
			ob.GetComponentsInChildren<Text>()[1].text = m.currentSize + "/" + m.maxSize;
			ob.GetComponent<Button>().onClick.AddListener(() => OnSelectMatch(m,k));
			ob.GetComponent<Image>().color = (k % 2 == 0) ? Color.white : Color.gray;
			ob.transform.SetParent(content);
		}
	}
	public void OnSelectMatch(MatchInfoSnapshot m,int n)
	{
		matchSelect = m;
		content.GetComponentsInChildren<Image>()[n].color = Color.yellow;
	}
	public void OnJoinMatch()
	{
		if (matchSelect == null) return;
		NetworkMaganer.Ins().JoinMatch(matchSelect.networkId);
	}
}
